package Day_21;

class Main {
    public static void main(String[] args) {
        int arr[] = new int[] { 5, 1, 9, 2, 5, 1, 7 };
        int n = arr.length;
        int LS[] = new int[n];
        int RS[] = new int[n];
        // put 1st value and last value in LS ang RS resp...
        LS[0] = 0;
        RS[n - 1] = 0;
        for (int i = 1; i < n; i++) {

            for (int j = i - 1; j >= 0; j--) {
                if (arr[j] < arr[i]) {
                    LS[i] = arr[j];
                    break;
                } else {
                    LS[i] = 0;
                }

            }

        }
        for (int i = 0; i < n - 1; i++) {

            for (int j = i + 1; j < n; j++) {
                if (arr[j] < arr[i]) {
                    RS[i] = arr[j];
                    break;
                } else {
                    RS[i] = 0;
                }

            }

        }
        // find the difference
        int diff = 0;
        int Max = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            if (LS[i] > RS[i]) {
                diff = LS[i] - RS[i];
            } else {
                diff = RS[i] - LS[i];
            }
            if (diff > Max) {
                Max = diff;
            }

        }

        System.out.println(Max);

    }
}
