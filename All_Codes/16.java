//Element with left side smaller and right side greater
class Compute {
    public int findElement(int arr[], int n) {
        for (int i = 1; i < n - 1; i++) {
            int j = 0;
            int flag = 0;
            int k = n - 1;
            int hlag = 0;
            // we check that all elements present left side to the arr[i] is less or not if
            // any element is greater then our given
            // condition becomes false hence break it and then flag is 1;
            while (j < i) {
                if (arr[j] > arr[i]) {
                    flag = 1;
                    break;
                }
                j++;
            }
            while (k > i) {
                if (arr[k] < arr[i]) {
                    hlag = 1;
                    break;
                }
                k--;
            }
            if (flag == 0 && hlag == 0)
                return arr[i];
        }
        return -1;
    }
}

class Main {
    public static void main(String[] args) {
        Compute s = new Compute();
        System.out.println(s.findElement(new int[] { 5, 6, 4, 1, 7, 12, 9, 1, 4, 1, 11, 5, 7, 1 }, 14));
    }
}