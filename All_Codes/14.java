
class Solution {
    public void zigZag(int a[], int n) {
        int flag = 1;
        int temp = 0;
        for (int i = 0; i < n - 1; i++) {
            if (a[i] > a[i + 1] && i % 2 == 0) {
                temp = a[i + 1];
                a[i + 1] = a[i];
                a[i] = temp;

            }
            if (a[i] < a[i + 1] && i % 2 != 0) {
                temp = a[i + 1];
                a[i + 1] = a[i];
                a[i] = temp;

            }
        }

        for (int i = 0; i < n; i++) {
            System.out.print(a[i] + " ");
        }
    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        s.zigZag(new int[] { 1, 4, 3, 2 }, 4);
    }
}