
//Next greater element -496
import java.util.*;

class Solution {
    /**
     * @param nums1
     * @param nums2
     * @return
     */
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        ArrayList<Integer> arr = new ArrayList<>(nums2.length);

        for (int i = 0; i < nums2.length; i++) {
            arr.add(nums2[i]);
        }
        for (int i = 0; i < nums1.length; i++) {

            int flag = 0;
            for (int j = arr.indexOf(nums1[i]); j < nums2.length; j++) {
                // int flag = 0;
                if (nums2[j] > nums1[i]) {
                    nums1[i] = nums2[j];
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                nums1[i] = -1;
            }
        }
        for (int i = 0; i < nums1.length; i++) {
            System.out.print(nums1[i]);
        }
        return nums1;

    }
}

class Main {
    public static void main(String[] args) {
        int[] arr1 = { 4, 1, 2 };
        int[] arr2 = { 1, 3, 4, 2 };

        Solution s = new Solution();
        int arr3[] = s.nextGreaterElement(arr1, arr2);
        System.out.print("[");
        for (int i = 0; i < arr3.length; i++) {
            System.out.print(arr3[i]);
        }
        System.out.println("]");
    }
}