
//check if Array is sorted and Rotated
import java.util.*;

class Solution {
    public boolean check(int[] nums) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > nums[(i + 1) % nums.length])
                count++;
        }
            if (count == 0) {
                return true;
            } else if (count > 1)
                return false;
            else if (nums[0] >= nums[nums.length - 1])
                return true;
            else
                return false;
        }

    }



class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.check(new int[] { 6, 7, 1, 1, 1, 5 }));
    }
}