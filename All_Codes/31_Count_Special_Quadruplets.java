class Solution {
    public int countQuadruplets(int[] nums) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums.length - i >= 4 && (nums[i] + nums[i + 1] + nums[i + 2] == nums[i + 3])) {
                count++;
            }
        }
        return count;

    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println("count" + s.countQuadruplets(new int[] { 3, 3, 6, 4, 5 }));
    }
}