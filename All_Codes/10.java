
//Maximum Products of Three Numbers
import java.util.*;

class Solution {
    public int maximumProduct(int[] nums) {
        Arrays.sort(nums);
        int product = nums[nums.length - 1] * nums[nums.length - 2] * nums[nums.length - 3];
        int negativep = nums[0] * nums[1] * nums[nums.length - 1];
        if (negativep > product)
            return negativep;
        return product;

    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.maximumProduct(new int[] { -100, -98, -1, 2, 3, 4 }));
    }
}
