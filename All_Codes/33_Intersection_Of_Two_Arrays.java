import java.util.*;

class Solution {
    int[] intersection(int[] arr1, int[] arr2) {

        HashSet<Integer> set = new HashSet<Integer>();
        for (int i = 0; i < arr1.length; i++) {
            set.add(arr1[i]);
        }

        ArrayList<Integer> al = new ArrayList<Integer>();

        for (int i = 0; i < arr2.length; i++) {
            if (set.contains(arr2[i])) {
                al.add(arr2[i]);
                set.remove(arr2[i]);
            }
        }

        int arr3[] = new int[al.size()];
        for (int i = 0; i < al.size(); i++) {
            arr3[i] = al.get(i);
        }
        return arr3;
    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        s.intersection(new int[] { 1, 2, 2, 1 }, new int[] { 2, 2 });
    }
}