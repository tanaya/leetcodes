import java.util.*;

class Solution {
    public static long[] productExceptSelf(int nums[], int n) {
        long prod = 1;
        ArrayList<Integer> al = new ArrayList<Integer>();

        long arr[] = new long[nums.length];
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0)
                prod = prod * nums[i];
            else
                al.add(i);

        }
        for (int i = 0; i < nums.length; i++) {
            if (al.contains(i) && al.size() == 1)
                arr[i] = prod;
            if (al.contains(i) && al.size() > 1)
                arr[i] = 0;
            if (!al.contains(i) && al.isEmpty())
                arr[i] = prod / nums[i];
            if (!al.contains(i) && !al.isEmpty())
                arr[i] = 0;
        }
        return arr;
    }
}

class Main {
    public static void main(String[] args) {
        int arr[] = new int[] { 0, 8, 6, 2, 4, 7, 9, 3, 9, 2, 8, 3, 0, 1, 7, 8, 9, 1, 5, 4, 9, 2, 5, 7, 4, 9, 9, 4 };
        Solution s = new Solution();
        s.productExceptSelf(arr, arr.length);

    }
}