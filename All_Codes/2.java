import java.util.*;

class Solution {
    public int firstRepeated(int[] arr, int n) {
        Map<Integer, Integer> indexMap = new HashMap<>();
        int minIndex = Integer.MAX_VALUE;
        int repeatingElement = -1;

        for (int i = 0; i < n; i++) {
            if (indexMap.containsKey(arr[i])) {
                if (indexMap.get(arr[i]) < minIndex) {
                    minIndex = indexMap.get(arr[i]);
                    repeatingElement = arr[i];
                }
            } else {
                indexMap.put(arr[i], i + 1);
            }
        }
        if (repeatingElement != -1) {
            return minIndex;
        } else {
            return -1;
        }
    }
}

class Client {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter size of array");
        int size = sc.nextInt();
        int[] arr = new int[size];

        System.out.println("Enter element");
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();
        System.out.println("Output: ");
        if (size == 1) {
            System.out.println("Only one element present");
        } else {
            int ret = obj.firstRepeated(arr, size);
            if (ret != -1) {
                System.out.println(ret);
            } else {
                System.out.println("All element appear only once");
            }
        }
    }

}