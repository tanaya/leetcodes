
//Missing Number:
import java.util.*;

class Solution {
    public int missingNumber(int[] nums) {
        /*
         * 
         * Arrays.sort(nums);
         * 
         * for (int i = 0; i <= nums.length; i++) {
         * int flag = 0;
         * for (int j = 0; j < nums.length; j++) {
         * if (nums[j] == i) {
         * flag = 1;
         * break;
         * }
         * }
         * if (flag == 0) {
         * return i;
         * }
         * 
         * }
         * return -1;
         */
        int m = nums.length * (nums.length + 1) / 2;
        int n = nums[nums.length - 1] * (nums[nums.length - 1] + 1) / 2;
        return m - n;

    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.missingNumber(new int[] { 3, 0, 1 }));
    }
}
