class Solution {
    public int[] runningSum(int[] nums) {
        int arr[] = new int[nums.length];
        arr[0] = nums[0];
        for (int i = 1; i < nums.length; i++) {
            arr[i] = arr[i - 1] + nums[i];
        }
        for (int i = 0; i < nums.length; i++) {
            System.out.println(arr[i]);
        }
        return arr;

    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.runningSum(new int[] { 1, 2, 3, 4 }));
    }
}
