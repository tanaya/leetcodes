class Compute {
    public String isSubset(long a1[], long a2[], long n, long m) {

        for (int i = 0; i < m; i++) {
            int flag = 0;
            for (int j = 0; j < n; j++) {
                if (a2[i] == a1[j]) {
                    flag = 1;
                }
            }
            if (flag == 0) {
                return "No";
            }
        }
        return "yes";

    }
}

class Main {
    public static void main(String[] args) {
        Compute s = new Compute();
        System.out.println(s.isSubset(new long[] { 10, 5, 2, 23, 19 }, new long[] { 19, 5, 3 }, 5, 3));
    }
}