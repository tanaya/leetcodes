import java.util.Arrays;

class Solution {

    public static long find_multiplication(int arr[], int brr[], int n, int m) {
        Arrays.sort(arr);
        Arrays.sort(brr);
        return arr[n - 1] * brr[0];

    }

}

class Main {
    public static void main(String[] args) {

        Solution s = new Solution();
        s.find_multiplication(new int[] { 5, 7, 9, 3, 6, 2 }, new int[] { 1, 2, 6, -1, 0, 9 }, 6, 6);

    }
}
