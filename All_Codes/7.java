import java.util.*;

class Solution {
    ArrayList<Long> arrangeOddAndEven(long a[], int n) {
        ArrayList<Long> l = new ArrayList<Long>();

        long data = Long.MAX_VALUE;
        int odd = 1;
        int Even = 0;
        for (int i = 0; i < a.length; i++) {
            l.add(data);
            l.add(data);
            if (a[i] % 2 == 0) {
                l.set(Even, a[i]);
                Even = Even + 2;
            } else {
                l.set(odd, a[i]);
                odd = odd + 2;
            }
        }
        // int ind=l.size()-1;
        // ArrayList<Long> al=new ArrayList<Long>();
        // for(int i=0;i<a.length;i++){
        // al.add(l.get(i));
        // l.subList(a.length, l.size()).clear();
        // }
        int ind = l.size() - 1;
        while (ind >= 0) {
            if (l.get(ind) == Long.MAX_VALUE) {
                l.remove(ind);
            }
            ind--;
        }

        return l;

    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        long[] arr = { -4, 83, 72, 86 };
        System.out.println(s.arrangeOddAndEven(arr, arr.length));
    }
}