import java.util.*;

class Solution {
    public int[] sortedSquares(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            nums[i] = nums[i] * nums[i];
        }
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            System.out.println(nums[i]);
        }
        return nums;

    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        s.sortedSquares(new int[] { 6, 2, 8, 4, 11, 13 });

    }
}