import java.util.*;

class Solution {
    public String longestCommonPrefix(String[] strs) {
        Arrays.sort(strs);
        ArrayList<Character> st = new ArrayList<>();
        
        for (int i = 0; i < strs[0].length(); i++) {
            int flag = 0;
            char ch = strs[0].charAt(i);
            for (int j = 0; j < strs.length; j++) {
                if (strs[j].charAt(i) == ch) {
                    flag = 1;
                } else {

                    flag = 0;
                    break;
                }

            }
            if (flag == 1) {
                st.add(ch);
            } else {
                break;
            }
        }
        char arr2[] = new char[st.size()];
        for (int i = 0; i < st.size(); i++) {
            arr2[i] = st.get(i);
        }
        return new String(arr2);
    }
}

class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter size of array:");
        int size = sc.nextInt();
        String[] arr = new String[size];
        for (int i = 0; i < size; i++) {

            arr[i] = sc.next();

        }
        Solution s = new Solution();
        System.out.println(s.longestCommonPrefix(arr));
        // System.out.print("[");
        // for (int i = 0; i < size; i++) {
        // System.out.print(arr[i] + " ");
        // }
        // System.out.println("]");
    }

}