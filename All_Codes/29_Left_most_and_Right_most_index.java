
//User function Template for Java

class pair {
    long first, second;

    public pair(long first, long second) {
        this.first = first;
        this.second = second;
    }
}

class Solution {

    public pair indexes(long v[], long x) {
        long first = -1;
        long secong = -1;
        int flag = 0;
        for (int i = 0; i < v.length; i++) {
            if (v[i] == x) {
                first = i;
                break;
            }
        }

        return new pair(first, secong);
    }
}

class Main {
    public static void main(String[] args) {

        Solution s = new Solution();
        pair p = s.indexes(new long[] { 1, 3, 5, 5, 5, 5, 7, 123, 125 }, 8);
        System.out.println(p.first);
        System.out.println(p.second);

    }
}
