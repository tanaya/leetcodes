class Main {
    public static void main(String[] args) {
        int arr[] = { 2, 4, 7, 8, 9, 10 };

        int temp = 0;
        for (int i = 0; i < arr.length - 1; i = i + 2) {
            temp = arr[i + 1];

            arr[i + 1] = arr[i];
            arr[i] = temp;

        }
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);

        }

    }
}