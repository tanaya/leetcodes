import java.util.*;

class Solutions {
    static ArrayList<Integer> leaders(int arr[], int n) {
        ArrayList<Integer> l = new ArrayList<Integer>(n);
        int max = arr[n - 1];
        l.add(max);
        for (int i = n - 2; i >= 0; i--) {
            if (arr[i] >= max) {
                max = arr[i];
                l.add(max);
            }
        }
        System.out.println(l);
        Collections.reverse(l);

        return l;

    }

}

class Main {
    public static void main(String[] args) {
        int arr[] = new int[] { 16, 17, 4, 3, 5, 2 };

        System.out.println(Solutions.leaders(arr, 6));
    }
}