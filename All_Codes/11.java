import java.util.*;

class Solution {
    public long findMinDiff(ArrayList<Integer> a, int n, int m) {
        Collections.sort(a);
        long val = Long.MAX_VALUE;
        for (int i = 0; i < n - m + 1; i++) {
            int j = i + m - 1;
            if (a.get(j) - a.get(i) < val) {
                val = a.get(j) - a.get(i);
            }
        }
        return val;
    }

}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        ArrayList<Integer> al = new ArrayList<>();
        al.add(1);
        al.add(3);
        al.add(4);
        al.add(7);
        al.add(9);
        al.add(9);
        al.add(12);
        al.add(56);
        System.out.println(s.findMinDiff(al, 8, 5));
    }
}
