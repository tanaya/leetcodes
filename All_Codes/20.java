//Buildings Receiving sunLight:
class Solution {

    public int longest(int arr[], int n) {
        int count = 0;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= max) {
                count++;
                max = arr[i];
            }
        }
        return count;

    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.longest(new int[] { 6, 2, 8, 4, 11, 13 }, 6));
    }
}