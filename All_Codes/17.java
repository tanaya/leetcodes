import java.util.*;

class Solution {
    void rearrange(int arr[], int n) {
        ArrayList<Integer> aln = new ArrayList<>();
        ArrayList<Integer> alp = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            if (arr[i] < 0) {
                aln.add(arr[i]);
            }
            if (arr[i] >= 0) {
                alp.add(arr[i]);
            }

        }
        int j = 0;
        int k = 0;
        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                arr[i] = alp.get(j);
                j++;
            } else {
                arr[i] = aln.get(k);
                k++;
            }

        }
        for (int i = 0; i < n; i++) {
            System.out.println(arr[i]);
        }

    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        s.rearrange(new int[] { 9, 4, -2, -1, 5, 0, -5, -3 }, 8);
    }
}