import java.util.*;

class Solution {
    int getPairsCount(int[] arr, int n, int k) {
        /*
         * int count = 0;
         * int sum = 0;
         * for (int i = 0; i < n; i++) {
         * for (int j = 0; j < n; j++) {
         * if (j == i)
         * break;
         * sum = arr[i] + arr[j];
         * if (sum == k)
         * count++;
         * }
         * }
         * return count;
         */
        int count = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            int key = k - arr[i];
            if (map.containsKey(key)) {
                count = count + map.get(key);
            }
            map.put(arr[i], map.getOrDefault(arr[i], 0) + 1);
        }
        return count;

    }
}

class Main {
    public static void main(String[] args) {
        Solution S = new Solution();
        int arr[] = new int[] { 1, 1, 1, 1 };

        System.out.println(S.getPairsCount(arr, 4, 2));
    }
}
