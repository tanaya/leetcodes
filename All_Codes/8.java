//Equillibrium point
class Solution {
    int pivotIndex(int[] nums) {
        int leftLength = 0;
        int rightLength = 0;
        for (int j = 0; j < nums.length; j++) {
            rightLength = rightLength + nums[j];
        }
        System.out.println(rightLength);

        for (int j = 0; j < nums.length; j++) {
            rightLength = rightLength - nums[j];
            if (leftLength == rightLength)
                return j;

            leftLength = leftLength + nums[j];
        }

        return -1;

    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        int arr[] = { 2, 1, -1 };
        System.out.println(s.pivotIndex(arr));
    }
}
