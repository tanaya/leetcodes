import java.util.*;

class Solution {
    public int removeDuplicates(int[] nums) {
        int j = 0;
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] != nums[i + 1]) {
                nums[j++] = nums[i];
            }
        }
        nums[j] = nums[nums.length - 1];
        for (int i = 0; i < nums.length - 1; i++) {
            System.out.print(nums[i]);
        }

        return j + 1;
    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.removeDuplicates(new int[] { 0, 1, 2, 3 }));
    }
}