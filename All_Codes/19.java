// Apply Operations to an Array:
class Solution {
    public int[] applyOperations(int[] nums) {
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == nums[i + 1]) {
                nums[i] = nums[i] * 2;
                nums[i + 1] = 0;
            } else {
                continue;
            }

        }

        int j = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > 0) {
                nums[j] = nums[i];
                j++;

            }

        }

        for (int i = j; i < nums.length; i++) {

            nums[i] = 0;

        }

        return nums;

    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        s.applyOperations(new int[] { 0, 1 });
    }
}