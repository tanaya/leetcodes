import java.util.*;

class Solution {
    public int singleNumber(int[] nums) {
        int ans = 0;
        for (int i = 0; i < nums.length; i++) {
            ans ^= nums[i];
        }

        return ans;
    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter size of array");
        int size = sc.nextInt();
        int arr[] = new int[size];
        System.out.println("Enter Elements");
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }
        int ans = s.singleNumber(arr);
        System.out.println(ans);
    }
}