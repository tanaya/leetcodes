class Solution {
    public int[] plusOne(int[] digits) {
        for (int i = digits.length - 1; i >= 0; i--) {
            if (digits[i] < 9) {
                ++digits[i];
                return digits;
            }
            digits[i] = 0;

        }
        int[] arr = new int[digits.length + 1];
        arr[0] = 1;

        return arr;

    }
}

class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
        int[] arr = s.plusOne(new int[] { 1, 9, 9 });
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i] + " ");

        }
    }
}